import { NiKreationPage } from './app.po';

describe('ni-kreation App', function() {
  let page: NiKreationPage;

  beforeEach(() => {
    page = new NiKreationPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
